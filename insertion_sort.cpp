#include <iostream>

using namespace std;

// sort A using insertion sort inplace.
void insertion_sort(int A[], int n)
{
    for (int j = 1; j < n; j++)
    {
        int i = j - 1;
        int key = A[j];

        while (i >= 0 && A[i] > key)
        {
            A[i + 1] = A[i];
            i--;
        }

        A[i + 1] = key;
    }
}

int main()
{
    int n, arr[20];

    cout << "Enter n: ";
    cin >> n;
    if (n > 20)
    {
        cout << "n cannot be > 20.";
        return 0;
    }

    cout << "Enter " << n << " numbers\n";
    for(int i = 0; i < n; i++)
        cin >> arr[i];

    insertion_sort(arr, n);

    cout << "Sorted list is \n";
    for(int i = 0; i < n; i++)
        cout << arr[i] << " ";
    cout <<"\n";

    return 0;
}
