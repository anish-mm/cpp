// This is a preprocessor directive.
#include <iostream>

int main()
{
    std::cout << "This is the first line.";
    std::cout << "This is shown in the same line.";
    std::cout << "\n";
    std::cout << "Now this is shown in a new line.";
}
