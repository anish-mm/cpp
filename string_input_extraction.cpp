#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main()
{
    string name;

    cout << "What is your name? \n";
    getline(cin, name);
    cout << "Hi " << name << "! Pleasure to meet you.\n";

    string mystr;
    float price;

    /* Decouple input process and interpretation of input as data.
       This allows us to further process the input while keeping The
       input process standard.
    */
    cout << "Enter price: ";
    getline(cin, mystr);
    stringstream(mystr) >> price;
    cout << "The price is " << price;
    return 0;
}
