#include <iostream>

#define PI 3.14

using namespace std;

int main()
{
    int i;

    i = (int) PI; // C-style type casting.
    cout << "i = " << i << endl;

    i = 0;
    i = int(PI); // C++ style type casting.
    cout << "i = " << i;
}
