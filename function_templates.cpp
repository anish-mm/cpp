#include <iostream>
#include <string>

using namespace std;

template <class T, class U>
bool are_equal(T a, U b)
{
    return (a == b);
}

template <class T, int N>
T fixed_mul(T val)
{
    return val * N;
}

int main()
{
    // Demonstrating the use of function tempates.
    cout << are_equal(10, 10.0) << "\n";

    int n;
    cin >> n;
    // Multiply by 2.
    cout << "double of n = " << fixed_mul<int, 2>(n) << "\n";
    cout << "triple of n = " << fixed_mul<int, 3>(n) << "\n";

    return 0;
}
