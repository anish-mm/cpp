#include <iostream>
#include <sstream>
#include <string>

using namespace std;

int main()
{
    // if else statements.
    string mystr;
    int n;

    cout << "Enter n: ";
    getline(cin, mystr);
    stringstream(mystr) >> n;

    if (n > 0)
        cout << "n is positive.";
    else if (n < 0)
        cout << "n is negative.";
    else
        cout << "n is 0.";

    // loops.
    // while loop.
    auto tmp (n);
    while (tmp > 0)
        cout << tmp-- << ", ";
    cout << "\n";

    // do while loop
    tmp = n;
    do {
        cout << tmp-- << ", ";
    } while(tmp > 0);
    cout << "\n";

    // for loop
    for (int i = n; i > 0; i--) {
        cout << i << ", ";
    }
    cout << "\n";

    // for loop for range.
    string thisMonth {"May"};
    for (auto i : thisMonth) {
        cout << "[" << i << "] ";
    }
    cout << "\n";

    return 0;
}
