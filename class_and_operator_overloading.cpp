#include <iostream>

using namespace std;

// Class for cartesian vectors.
class CVector {
public:
    int x, y;
    CVector() {};
    CVector(int a, int b): x(a), y(b) {};
    CVector operator + (const CVector &);
};

CVector CVector :: operator + (const CVector &param)
{
    CVector temp;
    temp.x = x + param.x;
    temp.y = y + param.y;

    return temp;
}

int main()
{
    CVector v1(1, 3);
    CVector v2(3, 4);
    CVector result = v1 + v2;
    cout << "(" << result.x << ", " << result.y << ")";

    return 0;
}
