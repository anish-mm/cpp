#include <iostream>

using namespace std;

int main()
{
    int x; // No initial value specified. Will contain junk value.
    int a = 5; // Initial value set using c-like initialization.
    int b (6); // Initial value set using constructor initialization.
    int c {7}; // Uniform initialization.

    cout << "x = " << x << endl;
    cout << "a = " << a << endl;
    cout << "b = " << b << endl;
    cout << "c = " << c << endl;

    return 0;
}
