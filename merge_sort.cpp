#include <iostream>

using namespace std;

void merge(int A[], int mid, int n)
{
    // merge arrays A[0, ..., (mid - 1)] and
    // A[mid, ..., (n - 1)].

    // Create the array copies.
    int B[mid], C[n - mid];
    for (int i = 0; i < mid; i++)
        B[i] = A[i];
    for (int i = mid; i < n; i++)
        C[i - mid] = A[i];

    int i, j, k;
    i = j = k = 0;
    while (i < mid and j < n - mid)
        A[k++] = B[i] < C[j] ? B[i++] : C[j++];

    if (i == mid)
    {
        // B is copied completely. copy rest of C.
        while (j < n - mid)
            A[k++] = C[j++];
    }
    else
    {
        while (i < mid) {
            A[k++] = B[i++];
        }
    }
}

void merge_sort(int A[], int n)
{
    // array = A[0, ..., (n - 1)]. length = n.

    // base case. n == 1.
    if (n == 1)
        return;

    int mid = n/2;
    // sub-array1 = A[0, ..., (mid - 1)]. length = mid.
    merge_sort(A, mid);

    // sub-array2 = A[mid, ..., (n - 1)]. length = n - mid.
    merge_sort(A + mid, n - mid);

    merge(A, mid, n);
}

int main()
{
    int n;
    cout << "Enter n: ";
    cin >> n;

    int A[n];
    cout << "Enter " << n << " numbers:\n";
    for (int i = 0; i < n; i++)
        cin >> A[i];

    merge_sort(A, n);

    cout << "Sorted list:\n";
    for (int i = 0; i < n; i++)
        cout << A[i] << " ";

    return 0;
}
