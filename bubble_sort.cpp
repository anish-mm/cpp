#include <iostream>

using namespace std;

void bubble_sort(int A[], int n)
{
    // In each step, bubble the lowest element to the left most position.
    for (int i = 0; i < n - 1; i++)
        for (int j = n - 1; j > i; j--)
            if (A[j] < A[j - 1])
            {
                // swap.
                int temp = A[j];
                A[j] = A[j - 1];
                A[j - 1] = temp;
            }
}

int main()
{
    int n;
    cout << "Enter n: ";
    cin >> n;

    int A[n];
    cout << "Enter the " << n << " numbers:\n";
    for (int i = 0; i < n; i++)
        cin >> A[i];

    bubble_sort(A, n);

    cout << "The sorted list is\n";
    for (int i = 0; i < n; i++)
        cout << A[i] << " ";
    cout << "\n";
    
    return 0;
}
